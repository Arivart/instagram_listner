from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func
from instagram_database_tables import User, Post, SavedStory

# Класс для работы с БД
class DataBaseWorker:
    # Константы
    DATABASE_TYPE = "postgresql"
    DATABASE_DRIVER = "psycopg2"
    HOST = "localhost"
    PORT = 5432
    DATABASE_NAME = "InstagramDataBase"

    # Конструктор
    def __init__(self, login, password):
        self.__login = login
        self.__password = password
        self.__engine = create_engine(f"{self.DATABASE_TYPE}+{self.DATABASE_DRIVER}://{self.__login}:{self.__password}@{self.HOST}:{self.PORT}/{self.DATABASE_NAME}", echo=False)
        Session = sessionmaker(bind=self.__engine)
        self.__session = Session()

    # Получение всех пользователей
    def get_users(self):
        try:
            users = self.__session.query(User).order_by(User.name)
            return users
        except Exception:
            print(f"DataBaseWorker Error: Can't get users.")
            return []

    # Получение пользователей, которых нужно мониторить
    def get_listen_users(self):
        try:
            listen_users = self.__session.query(User).filter(User.listening==True).order_by(User.name)
            return listen_users
        except Exception:
            print(f"DataBaseWorker Error: Can't get listen users.")
            return []

    # Получение пользователя по имени
    def get_user(self, user_name) -> User:
        try:
            users = self.__session.query(User).filter(User.name==user_name)
            if not users.count() == 0:
                return users.first()
            else:
                print(f"DataBaseWorker Error: Can't get user: \"{user_name}\".")
                return None
        except Exception:
            print(f"DataBaseWorker Error: Can't get users.")
            return []

    # Добавление пользователя
    def append_user(self, user_name):
        try:
            user = User(name=user_name)
            self.__session.add(user)
            self.__session.commit()
        except Exception:
            print(f"DataBaseWorker Error: Can't create new user: \"{user_name}\".")

    # Удаление пользователя
    def remove_user(self, user_name):
        try:
            users = self.__session.query(User).filter(User.name==user_name)
            for user in users:
                self.__session.delete(user)
            self.__session.commit()
        except Exception:
            print(f"DataBaseWorker Error: Can't get user: \"{user_name}\".")
    
    # Изменение флага прослушивания
    def change_user_listening(self, user_name, value=True):
        try:
            users = self.__session.query(User).filter(User.name==user_name)
            for user in users:
                user.listening = value
            self.__session.commit()
        except Exception:
            print(f"DataBaseWorker Error: Can't get user: \"{user_name}\".")

    # Запись постов пользователя в БД
    def write_user_posts(self, user_name, posts):
        # Получение пользователя
        user = self.get_user(user_name)
        # Цикл по постам
        for post in posts:
            # Запись каждого поста
            self.__session.add(post)
        # Сохранение изменений в БД
        self.__session.commit()

    # Проучение постов пользователя
    def get_user_posts(self, user_name):
        # Получение пользователя
        user = self.get_user(user_name)
        try:
            # Получение постов пользователя
            posts = self.__session.query(Post).filter(Post.user==user.id)
            # Возврат списка постов
            return posts
        except Exception:
            print(f"DataBaseWorker Error: Can't get posts of user \"{user_name}\".")
            return None

    # Запись сохраненных историй в БД
    def write_user_saved_stories(self, user_name, saved_stories):
        # Получение пользователя
        user = self.get_user(user_name)
        # Цикл по сохраненным исторям
        for saved_story in saved_stories:
            # Запись каждой истории
            self.__session.add(saved_story)
        # Сохранение изменений в БД
        self.__session.commit()

    # Проучение сохраненных историй пользователя
    def get_user_saved_stories(self, user_name):
        # Получение пользователя
        user = self.get_user(user_name)
        try:
            # Получение сохраненных историй пользователя
            saved_stories = self.__session.query(SavedStory).filter(SavedStory.user==user.id)
            # Возврат списка сохраненных историй
            return saved_stories
        except Exception:
            print(f"DataBaseWorker Error: Can't get posts of user \"{user_name}\".")
            return None