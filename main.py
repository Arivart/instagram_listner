from database_worker import DataBaseWorker
from instagram_listner import InstagramListner

# Функция запуска приложения
def main():
    # Создания экземпляра класса работы с БД
    database_worker = DataBaseWorker("postgres", "arivart")
    # Создания экземпляра класса прослушивателя пользователей Instagram
    listner = InstagramListner(database_worker)
    # Прослушивание
    listner.listen()

# Запуск приложения
main()