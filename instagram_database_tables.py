# Описание таблиц базы данных с помощью sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Text, Boolean, ForeignKey, TIMESTAMP
from sqlalchemy.dialects import postgresql

# Создание базового класса для описания записей таблиц
Base = declarative_base()

# Запись таблицы users
class User(Base):
    # Название таблицы в БД 
    __tablename__ = "users"
    # Уникальный идентификатор в БД (устанавливается автоматически)
    id = Column(Integer, primary_key=True)
    # Идентификатор пользователя в Instagram
    instagram_id = Column(Integer, nullable=True) 
    # Имя пользователя в Instagram
    name = Column(String(100))
    # Флаг необходимости прослушивания 
    listening = Column(Boolean, default=True)

# Запись таблицы posts
class Post(Base):
    # Название таблицы в БД
    __tablename__ = "posts"
    # Уникальный идентификатор в БД (устанавливается автоматически)
    id = Column(Integer, primary_key=True)
    # Идентификатор поста в Instagram
    instagram_id = Column(String(100), nullable=True) 
    # Флаг, содержит ли пост видео
    is_video = Column(Boolean)
    # Список текстовых описаний поста
    text = Column(postgresql.ARRAY(Text()), default=[])
    # Ссылка на пост
    url = Column(String(100))
    # Ссылка на пользователя в таблице users
    user = Column(Integer(), ForeignKey("users.id"))
    # Время публикации поста
    taken_at_timestamp = Column(Integer(), nullable=True)

# Запись таблицы saved_stories
class SavedStory(Base):
    # Название таблицы в БД
    __tablename__ = "saved_stories"
    # Уникальный идентификатор в БД (устанавливается автоматически)
    id = Column(Integer, primary_key=True)
    # Идентификатор поста в Instagram
    instagram_id = Column(String(100), nullable=True) 
    # Ссылка на стори
    url = Column(String(100))
    # Текстовое описание
    title = Column(String(1000))
    # Ссылка на пользователя в таблице users
    user = Column(Integer(), ForeignKey("users.id"))

# Преобразование json-объекта поста в экземпляр класса для sqlalchemy
def convert_json_object_to_post(root_url, user, json_object):
    post_id = json_object["node"]["id"]
    post_url = root_url + "p/" + json_object["node"]["shortcode"]
    post_is_video = json_object["node"]["is_video"]
    post_text_edges = json_object["node"]["edge_media_to_caption"]["edges"]
    post_text = []
    for post_text_edge in post_text_edges:
        post_text.append(post_text_edge["node"]["text"])
    taken_at_timestamp = json_object["node"]["taken_at_timestamp"]
    db_post = Post(instagram_id=post_id, is_video=post_is_video, url=post_url, text=post_text, user=user.database_id, taken_at_timestamp=taken_at_timestamp)
    return db_post

# Преобразование списка json-объектов постов в список экземпляров класса для sqlalchemy
def convert_json_array_to_posts(root_url, user, json_array):
    posts = []
    for json_object in json_array:
        post = convert_json_object_to_post(root_url, user, json_object)
        if post.taken_at_timestamp >  user.post_last_timestamp:
            posts.append(post)
    return posts

# Преобразование json-объекта сохраненной историй в класс для sqlalchemy
def convert_json_object_to_saved_story(root_url, user, json_object):
    saved_story_id = json_object["node"]["id"]
    saved_story_url = root_url + "stories/highlights/" + saved_story_id
    saved_story_title = json_object["node"]["title"] 
    db_saved_story = SavedStory(instagram_id=saved_story_id, url=saved_story_url, title=saved_story_title, user=user.database_id)
    return db_saved_story

# Преобразование списка json-объектов сохраненных историй в список экземпляров класса для sqlalchemy
def convert_json_array_to_saved_stories(root_url, user, json_array):
    saved_stories = []
    for json_object in json_array:
        saved_story = convert_json_object_to_saved_story(root_url, user, json_object)
        # Проверка на существование сохраненной истории в БД
        if user.saved_stories_instagram_ids.count(saved_story.instagram_id) == 0:
            saved_stories.append(saved_story)
    return saved_stories