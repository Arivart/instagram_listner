import json
import requests
from lxml import html
from sqlalchemy.sql.sqltypes import Boolean
from database_worker import DataBaseWorker
from instagram_database_tables import convert_json_array_to_posts, convert_json_array_to_saved_stories

# Класс для хранения данных о пользователе
class InstagramUser:
    # Конструктор
    def __init__(self, database_id, user_name):
        # Уникальный идентификатор в БД (PK)
        self.database_id = database_id
        # Имя пользователя в Instagram
        self.name = user_name
        # Идентификатор пользователя в Instagram
        self.instagram_id = 0
        # Список считанных постов
        self.geted_posts = []
        # Общее количество постов у пользователя
        self.posts_count = 0
        # Список считанных сохраненных историй
        self.geted_saved_stories = []
        # Идентификатор конца страницы с постами (для счииывания)
        self.end_cursor = ""
        # Время публикации последнего полученного поста
        self.post_last_timestamp = 0
        # Список id в Instagram для сохраненных историй
        self.saved_stories_instagram_ids = []

# Класс прослушивателя пользователей Instagram
class InstagramListner:
    # Корневой url для обращения
    ROOT_URL = "https://www.instagram.com/"
    # Описание для сервера от какого типа клиента исходит запрос
    USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36"
    # Хеш запроса постов в API GraphQL для Instagram
    POSTS_QUERY_HASH = "32b14723a678bd4628d70c1f877b94c9"
    # Хеш запроса сохраненных историй в API GraphQL для Instagram
    SAVED_STORIES_QUERY_HASH = "d4d88dc1500312af6f937f7b804c68c3"
    # Максимальное количество возвращаемых постов за раз (50)
    MAX_POSTS_COUNT_IN_REQUESTS = 50

    # Конструктор
    def __init__(self, data_base_worker: DataBaseWorker):
        # Пустой список пользователей
        self.users = []
        # Сохрание экземпляра класса для работы с БД
        self.__data_base_worker = data_base_worker
        # Получение списка пользователей, которых надо слушать, из БД
        users = self.__data_base_worker.get_listen_users()
        # Формирование списка пользователей
        print("Users:")
        for i, user in enumerate(users):
            self.users.append(InstagramUser(user.id, user.name))
            print(i + 1 , user.name)
        print("")
    
    # Прослушивание всех пользователей
    def listen(self):
        for user in self.users:
            self._listen_user(user)    

    # Прослушивание конкретного пользователя
    def _listen_user(self, user: InstagramUser):
        print(f"User name: {user.name}")
        # Инициализация пустого списка постов
        user.geted_posts = []
        # Определение времени последнего поста в БД
        user_posts = self.__data_base_worker.get_user_posts(user.name)
        user.post_last_timestamp = 0
        for user_post in user_posts:
            if user.post_last_timestamp < user_post.taken_at_timestamp:
                user.post_last_timestamp = user_post.taken_at_timestamp
        # Инициализация пустого списка сохраненных историй
        user.geted_saved_stories = []
        # Получение списка сохраненных историй пользователя
        user.saved_stories_instagram_ids = []
        user_saved_stories = self.__data_base_worker.get_user_saved_stories(user.name)
        for user_saved_story in user_saved_stories:
            user.saved_stories_instagram_ids.append(user_saved_story.instagram_id)
        # Получение страницы пользователя и извлечение информации:
        # - id пользователя
        # - суммарное количество постов
        # - первые двенадцать постов
        # - end_cursor для следующего запроса постов
        find_id = self._get_instagram_user_page(user)
        if not find_id:
            print("Error. Don't find user id.")
            return
        else:
            # Если после анализа страницы нет новых постов, то дальше считывать нет смысла
            if len(user.geted_posts) > 0:
                # Считывание всех оставшихся постов
                self._get_user_posts(user)
            # Запись постов в БД
            self._write_user_posts(user)
            # Вывод сообщения о количестве новых постов
            print("=======================================")
            print(f"Get {len(user.geted_posts)} new posts.")
            print("=======================================")
            # Считывание сохраненных историй
            self._get_user_saved_stories(user)
            # Запись сохраненных историй в БД
            self._write_user_saved_stories(user)
             # Вывод сообщения о количестве новых сохраненных историй
            print("=======================================")
            print(f"Get {len(user.geted_saved_stories)} new saved stories.")
            print("=======================================")
            print("")

    # Получение блока данных json на странице пользователя в переменной javascript window._sharedData
    def _get_shared_data_from_page(self, html_text):
        # Блок данных json
        json_text = ""
        # Разбор html-страницы
        page_tree = html.fromstring(html_text)
        # Получение всех тегов script c параметром type="text/javascript"
        javascripts = page_tree.xpath('//script[@type="text/javascript"]')
        # Для каждого тега (поиск нужного)
        for javascript in javascripts:
            # Получение текста внутри тега
            text_list = javascript.xpath('.//text()')
            for text in text_list:
                # Если длина текста меньше 21 символа, то это точно не нужный тег
                if len(text) >= 21:
                    # Поиск нужного тега по началу текста
                    if text[0:21] == "window._sharedData = ":
                        # Получение блока данных в формате json
                        json_text = text[21:-1]
        if len(json_text) == 0:
            print("InstagramListner Error: Can't find json data on page.")
        return json_text

    # Получение и разбор страницы пользователя в Instagram
    def _get_instagram_user_page(self, user: InstagramUser) -> bool:
        # Отправка запроса
        response = requests.get(self.ROOT_URL + user.name, headers = {"User-agent": self.USER_AGENT})
        print(response.url)
        if response.url == self.ROOT_URL + "accounts/login/":
            print("InstagramListner Error: Get login page.")
            return
        print(f"Code: {response.status_code}. Description: {response.reason}")
        # Если получен ответ с кодом [200 ОК]
        if response.status_code == 200:
            # Флаг, найден ли ID пользователя
            find_id = False
            # Выделение блока _sharedData на странице
            shared_data = self._get_shared_data_from_page(response.text)
            # Преобразование из строки json в объекты python
            json_data = json.loads(shared_data)
            # Извлечение из иерархии объектов ID пользователя
            user.instagram_id = json_data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["id"]
            print(f"User id: {user.instagram_id}")
            # Извлечение из иерархии объектов количества постов
            user.posts_count = json_data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["count"]
            print(f"Posts count: {user.posts_count}")
            # Извлечение из иерархии объектов первых 12 постов
            user.geted_posts = convert_json_array_to_posts(self.ROOT_URL, user, json_data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"])
            user.end_cursor = json_data["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["page_info"]["end_cursor"]
            # Установка флага, что найден ID пользователя
            find_id = True
        # Если получен другой ответ, кроме [200 ОК]
        else:
            print("InstagramListner Error: Don't find user id.")
            return False
        # Если не удалось найти ID пользователя, вывод сообщения об ошибке
        if not find_id:
            return False
        else:
            return True

    # Получение постов пользователя
    def _get_user_posts(self, user: InstagramUser):
        # Расчет количества итераций, для получения всех постов
        iterations = [self.MAX_POSTS_COUNT_IN_REQUESTS for i in range(int((user.posts_count - len(user.geted_posts))/self.MAX_POSTS_COUNT_IN_REQUESTS))]
        if (user.posts_count - len(user.geted_posts))%self.MAX_POSTS_COUNT_IN_REQUESTS > 0: 
            iterations.append((user.posts_count - len(user.geted_posts))%self.MAX_POSTS_COUNT_IN_REQUESTS)
        # Проход по полученным итерациям
        for i, it in enumerate(iterations):
            # Формирование url запроса
            get_posts_url = "graphql/query/?query_hash=" + self.POSTS_QUERY_HASH + "&variables="
            # Установка параметров запроса
            variables = {}
            variables["id"] = user.instagram_id
            variables["first"] = it
            variables["after"] = user.end_cursor
            get_posts_url += json.dumps(variables).replace(" ", "")
            # Отправка запроса
            response = requests.get(self.ROOT_URL + get_posts_url, headers = {"User-agent": self.USER_AGENT})
            print(f"Request {i + 1} from {len(iterations)}")
            print(response.url)
            print(f"Code: {response.status_code}. Description: {response.reason}")
            # Если получен ответ с кодом [200 ОК]
            if response.status_code == 200:
                # Преобразование из строки json в объекты python
                json_data = json.loads(response.text)
                # Текущее количество считанных постов
                gated_posts_count = len(user.geted_posts)
                # Извлечение из иерархии объектов списка постов
                user.geted_posts += convert_json_array_to_posts(self.ROOT_URL, user, json_data["data"]["user"]["edge_owner_to_timeline_media"]["edges"])
                # Извлечение из иерархии объектов параметра end_cursor для страницы
                user.end_cursor = json_data["data"]["user"]["edge_owner_to_timeline_media"]["page_info"]["end_cursor"]
                # Проверка, найдены ли новые посты. Если нет, то дальше делать запросы нет смысла
                if gated_posts_count == len(user.geted_posts):
                    break; 
            # Если получен ответ с кодом [429]
            elif response.status_code == 429:
                print("InstagramListner Error: Code 429 - blocked from server.")
                break
            # Если получен другой ответ, кроме [200 ОК] или [429]
            else:
                print("InstagramListner Error: Don't get the posts.")
                break

    # Запись полученных постов в БД
    def _write_user_posts(self, user: InstagramUser):
        self.__data_base_worker.write_user_posts(user.name, user.geted_posts)


    def _get_user_saved_stories(self, user: InstagramUser):
        # Формирование url запроса
        get_saved_stories_url = "graphql/query/?query_hash=" + self.SAVED_STORIES_QUERY_HASH + "&variables="
        # Установка параметров запроса
        variables = {}
        variables["user_id"] = user.instagram_id
        variables["include_chaining"] = False
        variables["include_reel"] = False
        variables["include_suggested_users"] = False
        variables["include_logged_out_extras"] = False
        variables["include_highlight_reels"] = True
        variables["include_live_status"] = False
        get_saved_stories_url += json.dumps(variables).replace(" ", "")
        # Отправка запроса
        response = requests.get(self.ROOT_URL + get_saved_stories_url, headers = {"User-agent": self.USER_AGENT})
        print(response.url)
        print(f"Code: {response.status_code}. Description: {response.reason}")
        # Если получен ответ с кодом [200 ОК]
        if response.status_code == 200:
            # Преобразование из строки json в объекты python
            json_data = json.loads(response.text)
            # Извлечение из иерархии объектов списка сохраненных историй
            user.geted_saved_stories += convert_json_array_to_saved_stories(self.ROOT_URL, user, json_data["data"]["user"]["edge_highlight_reels"]["edges"])
        # Если получен ответ с кодом [429]
        elif response.status_code == 429:
            print("InstagramListner Error: Code 429 - blocked from server.")
        # Если получен другой ответ, кроме [200 ОК] или [429]
        else:
            print("InstagramListner Error: Don't get the posts.")

    # Запись полученных сохраненных историй в БД
    def _write_user_saved_stories(self, user: InstagramUser):
        self.__data_base_worker.write_user_saved_stories(user.name, user.geted_saved_stories)
